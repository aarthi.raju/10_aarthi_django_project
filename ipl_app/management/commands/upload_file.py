from django.core.management.base import BaseCommand, CommandError
import csv
from ipl_app.models import Matches, Deliveries


class Command(BaseCommand):
    help = 'Adding csv to db'

    def add_arguments(self, parser):
        parser.add_argument('deliveries', nargs='+', type=str)

    def handle(self, *args, **options):
        '''for filename in options['matches']:
            print(filename)
            csv_filename='static/'+str(filename)+'.csv'
            print(csv_filename)
            with open(csv_filename) as csv_filename:
                matches_reader = csv.DictReader(csv_filename)
                matches_array=[]
                for match in matches_reader:
                    m = Matches()
                    m.id = match['id']
                    m.season = match['season']
                    m.city = match['city']
                    m.date = match['date']
                    m.team1 = match['team1']
                    m.team2 = match['team2']
                    m.toss_winner = match['toss_winner']
                    m.toss_decision = match['toss_decision']
                    m.result = match['result']
                    m.dl_applied = match['dl_applied']
                    m.winner = match['winner']
                    m.win_by_runs = match['win_by_runs']
                    m.win_by_wickets = match['win_by_wickets']
                    m.player_of_match = match['player_of_match']
                    m.venue = match['venue']
                    m.umpire1 = match['umpire1']
                    m.umpire2 = match['umpire2']
                    m.umpire3 = match['umpire3']
                    matches_array.append(m)
                print(matches_array)
                Matches.objects.bulk_create(matches_array)'''
        for filename in options['deliveries']:
            print(filename)
            csv_filename = 'static/' + str(filename) + '.csv'
            print(csv_filename)
            with open(csv_filename) as csv_filename:
                deliveries_reader = csv.DictReader(csv_filename)
                deliveries_array = []
                for delivery in deliveries_reader:
                    d = Deliveries()
                    d.match_id = delivery['match_id']
                    d.inning = delivery['inning']
                    d.batting_team = delivery['batting_team']
                    d.bowling_team = delivery['bowling_team']
                    d.over = delivery['over']
                    d.ball = delivery['ball']
                    d.batsman = delivery['batsman']
                    d.non_striker = delivery['non_striker']
                    d.bowler = delivery['bowler']
                    d.is_super_over = delivery['is_super_over']
                    d.wide_runs = delivery['wide_runs']
                    d.bye_runs = delivery['bye_runs']
                    d.legbye_runs = delivery['legbye_runs']
                    d.noball_runs = delivery['noball_runs']
                    d.penalty_runs = delivery['penalty_runs']
                    d.batsman_runs = delivery['batsman_runs']
                    d.extra_runs = delivery['extra_runs']
                    d.total_runs = delivery['total_runs']
                    d.player_dismissed = delivery['player_dismissed']
                    d.dismissal_kind = delivery['dismissal_kind']
                    d.fielder = delivery['fielder']
                    deliveries_array.append(d)
                print(deliveries_array)
                Deliveries.objects.bulk_create(deliveries_array)
