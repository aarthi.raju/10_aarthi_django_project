from django.db import models


# Create your models here.
class Matches(models.Model):
    id = models.CharField(max_length=30, primary_key=True)
    season = models.CharField(max_length=30)
    city = models.CharField(max_length=30)
    date = models.CharField(max_length=30)
    team1 = models.CharField(max_length=30)
    team2 = models.CharField(max_length=30)
    toss_winner = models.CharField(max_length=30)
    toss_decision = models.CharField(max_length=30)
    result = models.CharField(max_length=30)
    dl_applied = models.CharField(max_length=30)
    winner = models.CharField(max_length=30)
    win_by_runs = models.CharField(max_length=30)
    win_by_wickets = models.CharField(max_length=30)
    player_of_match = models.CharField(max_length=30)
    venue = models.CharField(max_length=30)
    umpire1 = models.CharField(max_length=30)
    umpire2 = models.CharField(max_length=30)
    umpire3 = models.CharField(max_length=30)


class Deliveries(models.Model):
    match_id = models.CharField(max_length=30)
    inning = models.CharField(max_length=30)
    batting_team = models.CharField(max_length=50)
    bowling_team = models.CharField(max_length=50)
    over = models.CharField(db_column='over_', max_length=30)
    ball = models.CharField(max_length=30)
    batsman = models.CharField(max_length=50)
    non_striker = models.CharField(max_length=50)
    bowler = models.CharField(max_length=50)
    is_super_over = models.CharField(max_length=30)
    wide_runs = models.CharField(max_length=30)
    bye_runs = models.CharField(max_length=30)
    legbye_runs = models.CharField(max_length=30)
    noball_runs = models.CharField(max_length=30)
    penalty_runs = models.CharField(max_length=30)
    batsman_runs = models.CharField(max_length=30)
    extra_runs = models.CharField(max_length=30)
    total_runs = models.CharField(max_length=30)
    player_dismissed = models.CharField(max_length=50)
    dismissal_kind = models.CharField(max_length=50)
    fielder = models.CharField(max_length=50)
