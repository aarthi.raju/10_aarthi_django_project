from django.shortcuts import render
from ipl_app.models import Matches,Deliveries
from django.http import HttpResponse
from django.db.models import Count, Sum, FloatField
from django.db.models.functions import Cast
import json
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
from django.conf import settings


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


# Create your views here.
@cache_page(CACHE_TTL)
def home(request):
    return render(request, 'home.html')


@cache_page(CACHE_TTL)
def no_of_matches_played_per_year(request):
    queryset = Matches.objects.all().values('season').annotate(total=Count('id'))
    return HttpResponse(queryset)


@cache_page(CACHE_TTL)
def matches_won_overall_years(request):
    queryset = Matches.objects.all().values('season','winner').annotate(total=Count('id')).order_by('total').exclude(winner='')
    return HttpResponse(queryset)


@cache_page(CACHE_TTL)
def extra_runs_conceded(request):
    queryset = Deliveries.objects.values('bowling_team').annotate(extra_runs=Sum('extra_runs')).filter(match_id__in=Matches.objects.values('id').filter(season='2016'))
    return HttpResponse(queryset)


@cache_page(CACHE_TTL)
def top_economical_bowlers(request):
    queryset = Deliveries.objects.filter(match_id__in = Matches.objects.filter(season = '2015')).values('bowler').annotate(eco =Cast((Sum('total_runs')-Sum('bye_runs'))*6/Count('extra_runs'),  output_field = FloatField())).order_by('eco')
    return HttpResponse(queryset)


@cache_page(CACHE_TTL)
def highest_run_scorers(request):
    queryset = Deliveries.objects.values('batsman').annotate(batsman_runs=Sum('batsman_runs')).filter(match_id__in=Matches.objects.values('id').filter(season='2014')).order_by('-batsman_runs')[:10]
    return HttpResponse(queryset)


@cache_page(CACHE_TTL)
def plot_no_of_matches_played_per_year(request):
    queryset = Matches.objects.all().values('season').annotate(total=Count('id'))
    result_dict = json.dumps({'data':list(queryset)})
    return render(request, 'noofmatchesplayedperyear.html', context={"key":result_dict})


@cache_page(CACHE_TTL)
def plot_matches_won_overall_years(request):
    queryset = Matches.objects.all().values('season','winner').annotate(total=Count('id')).order_by('total').exclude(winner='')
    result_dict = json.dumps({'data': list(queryset)})
    return render(request, 'matches_won_overall_years.html', context={"key":result_dict})


@cache_page(CACHE_TTL)
def plot_extra_runs_conceded(request):
    queryset = Deliveries.objects.values('bowling_team').annotate(extra_runs=Sum('extra_runs')).filter(match_id__in=Matches.objects.values('id').filter(season='2016'))
    result_dict = json.dumps({'data': list(queryset)})
    return render(request, 'extra_runs_conceded.html', context={"key":result_dict})


@cache_page(CACHE_TTL)
def plot_top_economical_bowlers(request):
    queryset = Deliveries.objects.filter(match_id__in = Matches.objects.filter(season = '2015')).values('bowler').annotate(eco =Cast((Sum('total_runs')-Sum('bye_runs'))*6/Count('extra_runs'),  output_field = FloatField())).order_by('eco')
    result_dict = json.dumps({'data': list(queryset)})
    return render(request, 'top_economical_bowlers.html', context={"key":result_dict})


@cache_page(CACHE_TTL)
def plot_highest_run_scorers(request):
    queryset = Deliveries.objects.values('batsman').annotate(batsman_runs=Sum('batsman_runs')).filter(match_id__in=Matches.objects.values('id').filter(season='2014')).order_by('-batsman_runs')[:10]
    result_dict = json.dumps({'data': list(queryset)})
    return render(request, 'highest_run_scorers.html', context={"key":result_dict})
